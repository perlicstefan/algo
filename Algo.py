from flask import Flask, render_template, request
from main import GetClassOfInput

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/classify', methods=["POST"])
def classify():
    if request.method == 'POST':
        input = request.form.get("input_text")
        result = "Result: " + GetClassOfInput(input)
        return render_template("index.html", result = result)



if __name__ == '__main__':
    app.run()